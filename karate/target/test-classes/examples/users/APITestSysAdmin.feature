Feature: Login as SysAdmin and test GET requests

  Background: 
    * url 'https://sit-sc-phonicshub.esa.edu.au'
    * header Content-Type = 'application/json'

  Scenario: Login as Sys Admin and test GET requests
    * def user =
      """
      {
      "userName": "systemadminatesa@fred.com",
      "password": "SysP@ssw0rd1!"
      }
      """
    Given path 'api/v1/Auth/login'
    And request user
    When method POST
    Then status 200
    And print 'response---', response
    * def accessToken = response.token
 
    # AUTH
    # https://sit-sc-phonicshub.esa.edu.au/api/v1/Auth/GetSSOInfoByEmail
    # https://sit-sc-phonicshub.esa.edu.au/api/v1/Auth/GetTokenForSsoTicket
    # https://sit-sc-phonicshub.esa.edu.au/api/v1/Auth/CheckPasswordStrength
   
    Given path 'api/v1/Auth/GetSSOInfoByEmail'
    And header Authorization = 'Bearer ' + accessToken
    When request {email:='@esa.edu.au'}
    Then method GET
    Then status 200
    And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken

    # EMAIL
		# https://sit-sc-phonicshub.esa.edu.au/api/v1/EmailDomainWhiteLists
    # https://sit-sc-phonicshub.esa.edu.au/api/v1/EmailDomainWhiteLists/id
    
    Given path 'api/v1/EmailDomainWhiteLists'
    When method GET
    Then status 200
    And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
    Given path 'api/v1/EmailDomainWhiteLists/'
    When request {id:='1'}
    Then method GET
    And status 200
    And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
    # GENERAL
    # https://sit-sc-phonicshub.esa.edu.au/api/v1/General/States
    # https://sit-sc-phonicshub.esa.edu.au/api/v1/General/Sectors
    # https://sit-sc-phonicshub.esa.edu.au/api/v1/General/YearLevel
    # https://sit-sc-phonicshub.esa.edu.au/api/v1/General/Scopes
    # https://sit-sc-phonicshub.esa.edu.au/api/v1/General/DeliveryModes
    
    Given path 'api/v1/General/States/'
    When method GET
    Then status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
    Given path 'api/v1/General/Sectors/'
    When method GET
    Then status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
    Given path 'api/v1/General/YearLevel/'
    When request {withRoles:='true'}
    Then method GET
    And status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
    Given path 'api/v1/General/DeliveryModes/'
    When method GET
    Then status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
    Given path 'api/v1/General/Scopes/'
    When request {withRoles:='true'}
    Then method GET
    And status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
    # Permission/Role
    Given path 'api/v1/Permission/Role/'
    When request {roleId:='2d01d043-759a-4968-b4c5-54495336b758'}
    #2d01d043-759a-4968-b4c5-54495336b758
    #3e583f42-1f09-4614-97da-63b95a3ec8f3
    Then method GET
    And status 200
    And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
    #ROLE
    Given path 'api/v1/Roles/get/'
    When method GET
    Then status 200
    And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
        
    # USER
    # https://sit-sc-phonicshub.esa.edu.au/api/v1/User/userid
    # https://sit-sc-phonicshub.esa.edu.au/api/v1/User/UserScope
    # https://sit-sc-phonicshub.esa.edu.au/api/v1/User/EmailStatus
    # https://sit-sc-phonicshub.esa.edu.au/api/v1/User/EmailValidation
    
    Given path 'api/v1/User/98cdb66f-f6b3-4a46-a33a-fba15dc66072'
    When method GET
    Then status 200
   # And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
    Given path 'api/v1/User/UserScope/'
    When request {userId:='98cdb66f-f6b3-4a46-a33a-fba15dc66072'}
    Then method GET
    And status 200
   # And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    

    
   
   
    
   