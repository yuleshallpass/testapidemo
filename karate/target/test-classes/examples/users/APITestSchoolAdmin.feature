Feature: Test GET requests based on user

  Background: 
    * url 'https://sit-sc-phonicshub.esa.edu.au'
    * header Content-Type = 'application/json'

  Scenario: Login as School Admin and test GET requests
    * def user =
      """
      {
      "userName": "schooladminatesa@fred.com",
      "password": "P@ssw0rd1!"
      }
      """
    Given path 'api/v1/Auth/login'
    And request user
    When method POST
    Then status 200
    And print 'response---', response
    * def accessToken = response.token
    
    #CLASSROOM
    #http://sit-sc-phonicshub.esa.edu.au/api/v1/Classrooms/
    #http://sit-sc-phonicshub.esa.edu.au/api/v1/Classrooms/List/<schoolId>
		Given path 'api/v1/Classrooms/1'
    And header Authorization = 'Bearer ' + accessToken
    Then method GET
    And status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
		Given path 'api/v1/Classrooms/List'
    And request {schoolId:='1'}
    When method GET
    Then status 200
    And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
  	#STUDENT
  	#https://sit-sc-phonicshub.esa.edu.au/api/v1/Students/List/<schoolId>
		#https://sit-sc-phonicshub.esa.edu.au/api/v1/Students/List/<studenId>
    Given path 'api/v1/Students/1'
    When method GET
    Then status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
    Given path 'api/v1/Students/List'
    And request {schoolId:='1'}
    When method GET
    Then status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
		
		#all assessment list
		Given path 'api/v1/Assessment'
    When method GET
    Then status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
    Given path 'api/v1/Assessment/1'
    When method GET
    Then status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
    Given path 'api/v1/Assessment/Test'
		And request {testId:='1'}
    When method GET
    Then status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
		
		Given path 'api/v1/Assessment/GetSuggestedResources'
		And request {studentId:='1'}
    When method GET
    Then status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
    Given path 'api/v1/Assessment/GetAssessmentDashboardData'
		And request {teacherId:='eb53b387-c366-49a7-b93b-3965d80ed69d'}
    When method GET
    Then status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
    #can only access by teacher, permission feature 'api/v1/Assessment/StartAssessment' , 
    #no student code on current data??? can only access by teacher, permission feature  'api/v1/Assessment/Results/Detailed'
    
    Given path 'api/v1/Assessment/Class'
		And request {classId:='1'}
    When method GET
    Then status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
    Given path 'api/v1/Assessment/Student'
		And request {studentId:='1'}
    When method GET
    Then status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
    #Assessment/GenerateQRCode?studentCode=BYWYM4415
		Given path 'api/v1/Assessment/GenerateQRCode'
		And request {studentCode:='BYWYM4415'}
    When method GET
    Then status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
    #TODO
    #https://sit-sc-phonicshub.esa.edu.au/api/v1/Assessment/Preference?preferenceType=testtype&assessmentId=1
    
    #GENERAL
    Given path 'api/v1/General/States/'
    When method GET
    Then status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
    Given path 'api/v1/General/Sectors/'
    When method GET
    Then status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
    Given path 'api/v1/General/YearLevel/'
    When request {withRoles:='true'}
    Then method GET
    And status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
    Given path 'api/v1/General/DeliveryModes/'
    When method GET
    Then status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
    Given path 'api/v1/General/Scopes/'
    When request {withRoles:='true'}
    Then method GET
    And status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
    # Permission/Role
    Given path 'api/v1/Permission/Role/'
    When request {roleId:='3e583f42-1f09-4614-97da-63b95a3ec8f3'}
    Then method GET
    And status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
    #ROLE
    Given path 'api/v1/Roles/get/'
    When method GET
    And status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
        
    # USER
    Given path 'api/v1/User/98cdb66f-f6b3-4a46-a33a-fba15dc66072'
    When method GET
    Then status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
    Given path 'api/v1/User/UserScope/'
    When request {userId:='98cdb66f-f6b3-4a46-a33a-fba15dc66072'}
    Then method GET
    And status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken

		#TEST
		#https://sit-sc-phonicshub.esa.edu.au/api/v1/Test
		#https://sit-sc-phonicshub.esa.edu.au/api/v1/Test/1
		Given path 'api/v1/Test'
    When method GET
    Then status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
    Given path 'api/v1/Test/1'
    When method GET
    Then status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
		
		#SCHOOLS
		#https://sit-sc-phonicshub.esa.edu.au/api/v1/Schools/List?stateId=1&sectorId=3
		Given path 'api/v1/Schools/List'
    When request {stateId:='1', sectorId:='3'}
    Then method GET
    And status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
		
		#https://sit-sc-phonicshub.esa.edu.au/api/v1/Schools/1
		Given path 'api/v1/Schools/1'
    When method GET
    Then status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
		
		#TODO validate why response is 204
		#https://sit-sc-phonicshub.esa.edu.au/api/v1/Schools/GetTeachersBySchool?schoolId=1
		Given path 'api/v1/Schools/GetTeachersBySchool'
    When request {schoolId:='1'}
    Then method GET
    And status 204
    #And print 'response---', response
    #* header Authorization = 'Bearer ' + accessToken
