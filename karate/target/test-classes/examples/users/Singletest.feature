Feature: Test GET requests based on user

  Background: 
    * url 'https://sit-sc-phonicshub.esa.edu.au'
    * header Content-Type = 'application/json'

  Scenario: Login as Teacher and test GET requests
    * def user =
      """
      {
      "userName": "teachersimon@fred.com",
      "password": "Te@cher1!"
      }
      """
    Given path 'api/v1/Auth/login'
    And request user
    When method POST
    Then status 200
    And print 'response---', response
    * def accessToken = response.token
    
    #TODO
    #Given path 'api/v1/Assessment/StartAssessment'
    #Given path 'api/v1/Assessment/Results/Detailed'
    #Assessment/Results/Detailed/Excel
    #Assessment/Results/Detailed/
    
		#ASSESSMENT
    Given path 'api/v1/Assessment/AssessmentStatuses'
    And header Authorization = 'Bearer ' + accessToken
    When method GET
    Then status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
    #all assessment list
		Given path 'api/v1/Assessment'
    When method GET
    Then status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
    Given path 'api/v1/Assessment/1'
    When method GET
    Then status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
    Given path 'api/v1/Assessment/Test'
		And request {testId:='1'}
    When method GET
    Then status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
		
		Given path 'api/v1/Assessment/GetSuggestedResources'
		And request {studentId:='1'}
    When method GET
    Then status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
    Given path 'api/v1/Assessment/GetAssessmentDashboardData'
		And request {teacherId:='eb53b387-c366-49a7-b93b-3965d80ed69d'}
    When method GET
    Then status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
    #can only access by teacher, permission feature 'api/v1/Assessment/StartAssessment' , 
    #no student code on current data??? can only access by teacher, permission feature  'api/v1/Assessment/Results/Detailed'
    
    Given path 'api/v1/Assessment/Class'
		And request {classId:='1'}
    When method GET
    Then status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
    Given path 'api/v1/Assessment/Student'
		And request {studentId:='1'}
    When method GET
    Then status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
    #Assessment/GenerateQRCode?studentCode=BYWYM4415
		Given path 'api/v1/Assessment/GenerateQRCode'
		And request {studentCode:='BYWYM4415'}
    When method GET
    Then status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
    #https://sit-sc-phonicshub.esa.edu.au/api/v1/Assessment/Preference?preferenceType=exam&assessmentId=1
    Given path 'api/v1/Assessment/Preference'
    When request {assessmentId:='1', preferenceType:=''}
    Then method GET
    And status 200
    And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
   